﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for products
    /// </summary>
    public class Product
    {
        public Product(int productID, string name, decimal listPrice, short modelYear, ProductCategory category, ProductBrand brand) {
            ProductID = productID;
            Name = name;
            ListPrice = listPrice;
            ModelYear = modelYear;
            Category = category;
            Brand = brand;
        }

        public decimal ListPrice { get; private set; }
        public string Name { get; private set; }
        public int ProductID { get; private set; }
        public short ModelYear { get; private set; }
        public ProductCategory Category { get; private set; }
        public ProductBrand Brand { get; private set; }
    }
}
