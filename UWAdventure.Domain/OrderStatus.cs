﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    public enum OrderStatus
    {
        Pending = 1,
        Processing = 2,
        Rejected = 3,
        Completed = 4


    }

}
