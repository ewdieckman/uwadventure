﻿namespace UWAdventure.Data
{
    public interface IInventoryDAO
    {
        int GetInventory(int store_id, int product_id);
    }
}