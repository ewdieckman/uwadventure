﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IProductBrandDAO
    {
        ProductBrandDTO GetByID(int brand_id);
    }
}