﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for order items
    /// </summary>
    public class OrderItem
    {
        public OrderItem(Order order, Product product, int quantity, decimal price)
        {
            Order = order;
            Product = product;
            Quantity = quantity;
            Price = price;
        }

        public Order Order { get; private set; }
        public Product Product { get; private set; }
        public int Quantity { get; private set; }
        public decimal Price { get; private set; }

        /// <summary>
        /// Increases the quantity of a product already in the order
        /// </summary>
        /// <param name="quantityToIncrease">Amount to increase the quantity of the product in the order</param>
        /// <remarks>Throws an exception if the product isn't in the order</remarks>
        public void IncreaseQuantity(int quantityToIncrease)
        {
            Quantity += quantityToIncrease;
        }


    }
}
