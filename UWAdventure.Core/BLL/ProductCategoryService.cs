﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    /// <summary>
    /// Business logic for dealing with product categories
    /// </summary>
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IProductCategoryDAO _categoryDAO;

        public ProductCategoryService(IProductCategoryDAO categoryDAO)
        {
            _categoryDAO = categoryDAO;
        }

        /// <summary>
        /// returns a <see cref="ProductCategory"/> object for the specified category ID
        /// </summary>
        public ProductCategory GetByID(int category_id)
        {
            ProductCategoryDTO categoryDTO = _categoryDAO.GetByID(category_id);
            return DTOtoCategory(categoryDTO);
        }

        /// <summary>
        /// Converts an <see cref="ProductCategoryDTO"/> to <see cref="ProductCategory"/>
        /// </summary>
        private ProductCategory DTOtoCategory(ProductCategoryDTO categoryDTO)
        {
            ProductCategory category = new ProductCategory(categoryDTO.category_id, categoryDTO.category_name);

            return category;
        }

        /// <summary>
        /// Converts an <see cref="ProductCategory"/> to <see cref="ProductCategoryDTO"/>
        /// </summary>
        private ProductCategoryDTO CategoryToDTO(ProductCategory category)
        {
            ProductCategoryDTO categoryDTO = new ProductCategoryDTO()
            {
                category_id = category.CategoryID,
                category_name = category.Name
            };

            return categoryDTO;
        }
    }
}
