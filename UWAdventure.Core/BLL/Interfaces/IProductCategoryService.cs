﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface IProductCategoryService
    {
        ProductCategory GetByID(int category_id);
    }
}