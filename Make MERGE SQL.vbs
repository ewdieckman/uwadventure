'##  Creates MERGE SQL syntax for various scenarios
'##
'##  Author: Eric Dieckman
'##  Date: 11/24/2017
'##
'#########################

db = "UWAdventure"
'db = InputBox("Which database for the SQL parameters?", "Specify database")
tbl = InputBox("Which table to generate the AAPL classes from (include schema)?", "Specify table")
If db = "" OR tbl = "" Then Wscript.Quit


schema = Left(tbl, Instr(tbl, ".")-1)
tbl = Mid(tbl, Instr(tbl, ".")+1)

'Constants for recordsets
Public Const adUseClient = 3
Public Const adUseServer = 2
Public Const adForwardOnly = 0
Public Const adStateOpen = 1
Public Const adStateClosed = 0

'Constants for text files
Public Const adForReading = 1
Public Const adForWriting = 2
Public Const adForAppending = 8

'Constants for MsgBox

'Constants used for AD
Public Const ADS_SCOPE_SUBTREE = 2
Set objShell = WScript.CreateObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set chConn = CreateObject("ADODB.Connection")
chConn.Open "Provider=SQLNCLI11;Server=localhost;Database=" & db & ";Trusted_Connection=yes;"


Set rsCols = chConn.Execute("SELECT * FROM dbo.vwAllColumns WHERE TABLE_NAME='" & tbl & "' AND [SCHEMA_NAME]='" & schema & "' ORDER BY column_id;")

If rsCols.EOF Then
	Wscript.Echo "No table information found for " & schema & "." & tbl
	Wscript.Quit
End If

dto = ""
persist = ""
mapper = ""
service = ""
UNIQUEID = ""

'########################################################################
'###   DTO SECTION

'set the file name to DTO name (or table name, if not specified)
If dtoName = "" Then
	dtoFilename= tbl
	dtoName = tbl
Else
	dtoFilename =  dtoName
End If

'########################################################################
'###   MEMBERSTYLE-STYLE MERGE SECTION

membership =  VSTab(3) & "IDbCommand command = dao.GetDbCommand("""");" & vbCrLf & vbCrLf
AppendLine membership, VSTab(3) & "string commandText = String.Empty;", 1
AppendLine membership, VSTab(3) & "if (entity.COLLECTION.Count > 0)", 1
AppendLine membership, VSTab(3) & "{", 1
AppendLine membership, VSTab(4) & "commandText = ""SELECT * FROM (VALUES"";", 1
AppendLine membership, VSTab(4) & "string strValues = """";", 2
AppendLine membership, VSTab(4) & "//construct a Table Value Constructor as the SOURCE using the input values", 1
AppendLine membership, VSTab(4) & "for (Int32 i = 0; i < entity.COLLECTION.Count; i++)", 1
AppendLine membership, VSTab(4) & "{", 1
AppendLine membership, VSTab(5) & "if (strValues != """") { strValues += "",""; };", 1
AppendLine membership, VSTab(5) & "//Usually the first column name DOESN'T include a ""{0}"" as it's the primary key for the membership collection", 1
AppendLine membership, VSTab(5) & "strValues += String.Format(""(###MERGE_SELECT_FROM_VALUES###)"", i);",2
AppendLine membership, VSTab(5) & "//parameters for FROM values - be sure to include {0} and ""i"" in the parameter name", 1
AppendLine membership, "###SUPL_VAL_PARAMETERS###", 1
AppendLine membership, VSTab(4) & "}", 2
AppendLine membership, VSTab(4) & "strValues += "") AS supl_values(###SELECT_VALUES###)"";", 1
AppendLine membership, VSTab(4) & "commandText += strValues;", 1
AppendLine membership, VSTab(3) & "}", 1
AppendLine membership, VSTab(3) & "else", 1
AppendLine membership, VSTab(3) & "{", 1
AppendLine membership, VSTab(4) & "//no membership - still need a statement to return an empy result for merge to work properly",1
AppendLine membership, VSTab(4) & "commandText = ""SELECT ###SELECT_VALUES### [" & schema & "].[" & tbl &"] WHERE 1=0"";",1
AppendLine membership, VSTab(3) & "}", 2
AppendLine membership, VSTab(3) & "//parameter for foreign key that all membership values key back to", 1
AppendLine membership, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@PRIMARY_KEY_ID"", entity.PRIMARY_KEY));", 2
AppendLine membership, VSTab(3) & "commandText = @""MERGE [" & schema & "].[" & tbl & "] AS TARGET ", 1
AppendLine membership, VSTab(4) & "USING ("" + commandText + @"") AS SOURCE", 1
AppendLine membership, VSTab(4) & "ON TARGET.PRIMARY_KEY_ID = SOURCE.PRIMARY_KEY_ID AND TARGET.ANOTHER_KEY_ID = SOURCE.ANOTHER_KEY_ID", 1
AppendLine membership, VSTab(4) & "WHEN MATCHED THEN", 1
AppendLine membership, VSTab(4) & "UPDATE SET ###UPDATE_SQL###", 1
AppendLine membership, VSTab(4) & "WHEN NOT MATCHED BY TARGET THEN", 1
AppendLine membership, VSTab(4) & "INSERT (###SELECT_VALUES###) VALUES (###INSERT_SQL###)", 1
AppendLine membership, VSTab(4) & "WHEN NOT MATCHED BY SOURCE AND TARGET.PRIMARY_KEY_ID=@PRIMARY_KEY_ID THEN", 1
AppendLine membership, VSTab(4) & "DELETE;"";", 2
AppendLine membership, VSTab(3) & "command.CommandText = commandText;", 2
AppendLine membership, VSTab(3) & "dao.ExecuteNonQuery(command);", 2

'########################################################################
'###   SINGLE RECORD MERGE SECTION

singleValue = VSTab(3) & "string commandText = @""MERGE [" & schema & "].[" & tbl & "] AS TARGET " & vbCrLf
AppendLine singleValue, VSTab(4) & "USING (SELECT ###USING_VALUES###) AS SOURCE ", 1
AppendLine singleValue, VSTab(4) & "ON TARGET.PRIMARY_KEY_ID = SOURCE.PRIMARY_KEY_ID", 1
AppendLine singleValue, VSTab(4) & "WHEN MATCHED THEN", 1
AppendLine singleValue, VSTab(4) & "UPDATE SET ###UPDATE_SQL###", 1
AppendLine singleValue, VSTab(4) & "WHEN NOT MATCHED BY TARGET THEN", 1
AppendLine singleValue, VSTab(4) & "INSERT (###SELECT_VALUES###)", 1
AppendLine singleValue, VSTab(5) & "VALUES (###INSERT_SQL###);"";", 2
AppendLine singleValue, VSTab(3) & "IDbCommand command = dao.GetDbCommand(commandText);", 1
AppendLine singleValue, "###SIMPLE_PARAMETERS###", 1
AppendLine singleValue, VSTab(3) & "dao.ExecuteNonQuery(command);", 1

updateSQL = ""
insertSQL = ""
suplValues = ""
usingValues = ""
selectList = ""
While Not rsCols.EOF

	'dont do it for tmAdd, tmUpdate, which is what is typically used for persistence properties
	columnName = rsCols.Fields("COLUMN_NAME")
	dataType = rsCols.Fields("DATA_TYPE")
	maxLength = rsCols.Fields("CHARACTER_MAXIMUM_LENGTH")
	isIdentity = rsCols.Fields("is_identity")
	isComputed = rsCols.Fields("is_computed")
	isNullable = rsCols.Fields("is_nullable")
	collation_name = rsCols.Fields("collation_name")
	isUnique = rsCols.Fields("is_unique")

	m_oridinal = "m_ordinal_" & columnName

	'reset simpleParam, suplValParam - used once for each parameter, then reset.  daoCreateParam and daoUpdateParam hold running values
	simpleParam = ""
	suplValParam = ""
	dtoPropsTemp = ""
	If LCase(columnName) <> "tmadd" And LCase(columnName) <> "tmupdate" AND LCase(columnName) <> "tm_add" And LCase(columnName) <> "tm_update"  Then

		If Not isIdentity Then

			If selectList<> "" Then AppendLine selectList, ", ", 0
			AppendLine selectList, columnName, 0

			If suplValues<> "" Then AppendLine suplValues, ", ", 0
			AppendLine suplValues, "@" & columnName & "{0}", 0

			If usingValues<> "" Then AppendLine usingValues, ", ", 0
			AppendLine usingValues, "@" & columnName & " AS " & columnName, 0

			If updateSQL <> "" Then AppendLine updateSQL, ", ", 0
			AppendLine updateSQL, "TARGET." & columnName & "=SOURCE." & columnName, 0

			If insertSQL <> "" Then AppendLine insertSQL, ", ", 0
			AppendLine insertSQL, "SOURCE." & columnName, 0


			AppendLine simpleParam, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@" & columnName & """, entity." & columnName, 0
			AppendLine suplValParam, VSTab(5) & "command.Parameters.Add(dao.CreateParameter(String.Format(""@" & columnName & "{0}"",i), entity.COLLECTION[i]." & columnName, 0
			If Not IsNull(maxLength) AND dataType <> "text" AND Not ISNull(collation_name) Then
				AppendLine simpleParam, ", " & maxLength, 0
				AppendLine suplValParam, ", " & maxLength, 0
			End If

			If  Instr(LCase(columnName), "date") <> 0 Then
				AppendLine simpleParam, ", DbType." & dataType, 0
				AppendLine suplValParam, ", " & maxLength, 0
			End If


			AppendLine simpleParam, "));", 1
			AppendLine suplValParam, "));", 1
		
			mergeSimpleParam = mergeSimpleParam & simpleParam
			mergeSuplValParam = mergeSuplValParam & suplValParam
			
		End If
		
	End If

	If Not isIdentity Then
		If insertCol <> "" Then AppendLine insertCol, ", ", 0
		AppendLine insertCol, columnName, 0

		
	End If


	'replace uniqueID references in the files
	If isUnique Then
		UNIQUEID = columnName
	End If

	rsCols.MoveNext
Wend


membership = Replace(membership, "###MERGE_SELECT_FROM_VALUES###", suplValues)
membership = Replace(membership, "###SELECT_VALUES###", selectList)
membership = Replace(membership, "###SUPL_VAL_PARAMETERS###", mergeSuplValParam)
membership = Replace(membership, "###UPDATE_SQL###", updateSQL)
membership = Replace(membership, "###INSERT_SQL###", insertSQL)

singleValue = Replace(singleValue, "###USING_VALUES###", usingValues)
singleValue = Replace(singleValue, "###SELECT_VALUES###", selectList)
singleValue = Replace(singleValue, "###SIMPLE_PARAMETERS###", mergeSimpleParam)
singleValue = Replace(singleValue, "###UPDATE_SQL###", updateSQL)
singleValue = Replace(singleValue, "###INSERT_SQL###", insertSQL)


'########################################################################
'###   FINAL FILE WRITING
'###

strDesktop = objShell.SpecialFolders("Desktop")
' objFSO.CreateFolder(strDesktop & "\Models")
' objFSO.CreateFolder(strDesktop & "\Models\View")
' objFSO.CreateFolder(strDesktop & "\Models\Input")
' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\View\" & dtoFilename & ".vb", True)
' objFLwrite.WriteLine dto
' objFLwrite.Close

' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\Input\" & dtoCreateFilename & ".vb", True)
' objFLwrite.WriteLine dtoCreate
' objFLwrite.Close

' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\Input\" & dtoUpdateFilename & ".vb", True)
' objFLwrite.WriteLine dtoUpdate
' objFLwrite.Close

' objFSO.CreateFolder(strDesktop & "\Persistence")
' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Persistence\" & persistFilename & ".vb", True)
' objFLwrite.WriteLine persist
' objFLwrite.Close

' objFSO.CreateFolder(strDesktop & "\DataMappers")
' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\DataMappers\" & mapperFilename & ".vb", True)
' objFLwrite.WriteLine mapper
' objFLwrite.Close

' objFSO.CreateFolder(strDesktop & "\Services")
' Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Services\" & serviceFilename & ".vb", True)
' objFLwrite.WriteLine service
' objFLwrite.Close

Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\" & tbl & "-MERGE.vb", True)
objFLwrite.WriteLine membership
objFLwrite.WriteLine vbCrLf & vbCrLf & "'####################################'" & vbCrLf & vbCrLf
objFLwrite.WriteLine singleValue
objFLwrite.Close

'clean up'
chConn.Close

MsgBox "Done.  SQL file can be found here: " & strDesktop

'########################################################################
'###   FUNCTIONS
'###
Function SQL_to_CS_type(dataType)

	Select Case dataType
		Case "int"
			SQL_to_CS_type = "Int32"
		Case "smallint"
			SQL_to_CS_type = "Int16"
		Case "bigint"
			SQL_to_CS_type = "Int64"
		Case "bit"
			SQL_to_CS_type = "bool"
		Case "decimal"
			SQL_to_CS_type = "Decimal"
		Case "date", "datetime", "datetime2"
			SQL_to_CS_type = "DateTime"
		Case "varchar", "text", "nvarchar", "char"
			SQL_to_CS_type = "String"
		Case "float"
			SQL_to_CS_type = "Double"
		Case "uniqueidentifier"
			SQL_to_CS_type = "Guid"


	End Select

End Function

Function DefaultValue(dataType)

	Select Case dataType
		Case "int", "smallint", "bigint", "decimal", "float"
			DefaultValue = "-1"
		Case "bit"
			DefaultValue = "True"
		Case "date", "datetime", "datetime2", "uniqueidentifier"
			DefaultValue = "Nothing"
		Case "varchar", "text", "nvarchar", "char"
			DefaultValue = """"""
	End Select

End Function

'returns number of spaces based on the number of tabs (to mimic Visual Studio)
Function VSTab(num)
	str = ""
	For i = 1 To num
		str = str & "    "
	Next

	VSTab = str
End Function

'appends text to a string variable'
Sub AppendLine(inStr, strToAdd, num_carriage_return)
	If IsEmpty(num_carriage_return) Then num_carriage_return = 0

	inStr = inStr & strToAdd
	For i = 1 To num_carriage_return
		inStr = inStr & vbCrLf
	Next
End Sub
