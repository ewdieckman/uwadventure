﻿using System;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;
using Dapper;
using System.Data.SqlClient;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for orders 
    /// </summary>
    public class OrderDAO : IOrderDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public OrderDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }
        public void CreateOrder(OrderDTO orderDTO)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"UPDATE [sales].[orders] ,[customer_id]=@customer_id,[order_status]=@order_status,[order_date]=@order_date,[shipped_date]=@shipped_date,[store_id]=@store_id
                            ,[staff_id]=@staff_id  WHERE [order_number]=@order_number;";
                connection.Execute(sql, orderDTO);
            }
        }

        /// <summary>
        /// Returns a DTO of order information for the specified order number
        /// </summary>
        public OrderDTO GetByOrderNumber(int order_number)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [sales].[orders] WHERE order_number=@order_number;";
                OrderDTO order = connection.QuerySingleOrDefault<OrderDTO>(sql, new { order_number });
                if (order == null)
                {
                    //returning the static NOT FOUND object for OrderDTO.  Easy to compare, plus more obvious what is means:
                    // if (myorder = OrderDTO.NOTFOUND)  compared to  if (myorder == null) - the former is clearer as to what is happening
                    return OrderDTO.NOTFOUND;
                }
                return order;
            }
        }

        /// <summary>
        /// Saves an order record to the persistence store
        /// </summary>
        public void Save(OrderDTO order)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reserves an order number in the system for use in creating an order
        /// </summary>
        /// <remarks>Actually creates a record in the order table with a status of -999</remarks>
        public int ReserveOrderNumber()
        {

            // this is a poorly designed system - there should never be "fake data" insert into the database.
            // System should be redesigned to not use the order number as necessary into the Domain Model
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"INSERT INTO [sales].[orders] ([order_status],[order_date],[store_id],[staff_id])
                            VALUES -999,GETUTCDATE(),1,1);
                            SELECT SCOPE_IDENTITY();";

                return (int)connection.ExecuteScalar(sql);
                
            }
        }
    }
}
