﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for customers
    /// </summary>
    public class Customer
    {
        public Customer(int customerID, string first_name, string last_name, string phone, string email, string street, string city, string state, string zip)
        {
            CustomerID = customerID;
            FirstName = first_name;
            LastName = last_name;
            Phone = phone;
            Email = email;
            Street = street;
            City = city;
            State = state;
            ZipCode = zip;
        }

        public int CustomerID { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public string Street { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string ZipCode { get; private set; }
    }
}
