﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IStoreDAO
    {
        StoreDTO GetByID(int store_id);
    }
}