﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface IProductBrandService
    {
        ProductBrand GetByID(int brand_id);
    }
}