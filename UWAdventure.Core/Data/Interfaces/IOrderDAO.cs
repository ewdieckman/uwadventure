﻿using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IOrderDAO
    {
        void Save(OrderDTO order);
        OrderDTO GetByOrderNumber(int order_number);
        int ReserveOrderNumber();
    }
}