﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IProductDAO
    {
        ProductDTO GetByID(int product_id);
    }
}