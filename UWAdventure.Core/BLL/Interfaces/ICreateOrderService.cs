﻿using System;
using UWAdventure.Domain;
using UWAdventure.Events;

namespace UWAdventure.BLL
{
    public interface ICreateOrderService
    {
        event EventHandler<OrderCreatedEventArgs> OrderCreated;

        Order GetByOrderNumber(int order_number);
    }
}