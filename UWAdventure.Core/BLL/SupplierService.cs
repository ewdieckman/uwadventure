﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.BLL
{
    /// <summary>
    /// Business object responsible for interacting with suppliers
    /// </summary>
    /// <remarks>Not implemented - stub methods only for illustrative purposes</remarks>
    public class SupplierService
    {
    }
}
