﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IStaffDAO
    {
        StaffDTO GetByID(int staff_id);
        short GetPINforStaff(int staff_id);
    }
}