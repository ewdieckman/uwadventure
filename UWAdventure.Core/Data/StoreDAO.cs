﻿using System.Data.SqlClient;
using UWAdventure.Entities.Persistence;
using Dapper;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for stores
    /// </summary>
    public class StoreDAO : IStoreDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public StoreDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }

        public StoreDTO GetByID(int store_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [sales].[staffs] WHERE store_id=@store_id;";
                StoreDTO store = connection.QuerySingleOrDefault<StoreDTO>(sql, new { store_id });
                if (store == null)
                {
                    //returning the static NOT FOUND object for StoreDTO.  Easy to compare, plus more obvious what is means:
                    // if (mystore = StoreDTO.NOTFOUND)  compared to  if (mystore == null) - the former is clearer as to what is happening
                    return StoreDTO.NOTFOUND;
                }
                return store;
            }
        }
    }
}
