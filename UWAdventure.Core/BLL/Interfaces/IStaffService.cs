﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface IStaffService
    {
        Staff GetByID(int staff_id);
    }
}