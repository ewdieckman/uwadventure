﻿using System.Data.SqlClient;
using UWAdventure.Entities.Persistence;
using Dapper;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for staff
    /// </summary>
    public class StaffDAO : IStaffDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public StaffDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }
        
        public StaffDTO GetByID(int staff_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [sales].[staffs] WHERE staff_id=@staff_id;";
                StaffDTO staff = connection.QuerySingleOrDefault<StaffDTO>(sql, new { staff_id });
                if (staff == null)
                {
                    //returning the static NOT FOUND object for StaffDTO.  Easy to compare, plus more obvious what is means:
                    // if (mystaff = StaffDTO.NOTFOUND)  compared to  if (mystaff == null) - the former is clearer as to what is happening
                    return StaffDTO.NOTFOUND;
                }
                return staff;
            }
        }

        public short GetPINforStaff(int staff_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT pin FROM [sales].[staffs] WHERE staff_id=@staff_id;";
                short pin = (short)connection.ExecuteScalar(sql, new { staff_id });
                return pin;
            }
        }
    }
}
