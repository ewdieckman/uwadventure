﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    /// <summary>
    /// Business logic for dealing with product brands
    /// </summary>
    public class ProductBrandService : IProductBrandService
    {
        private readonly IProductBrandDAO _brandDAO;

        public ProductBrandService(IProductBrandDAO brandDAO)
        {
            _brandDAO = brandDAO;
        }

        /// <summary>
        /// returns a <see cref="ProductBrand"/> object for the specified brand ID
        /// </summary>
        public ProductBrand GetByID(int brand_id)
        {
            ProductBrandDTO brandDTO = _brandDAO.GetByID(brand_id);
            return DTOtoBrand(brandDTO);
        }

        /// <summary>
        /// Converts an <see cref="ProductBrandDTO"/> to <see cref="ProductBrand"/>
        /// </summary>
        private ProductBrand DTOtoBrand(ProductBrandDTO brandDTO)
        {
            ProductBrand brand = new ProductBrand(brandDTO.brand_id, brandDTO.brand_name);

            return brand;
        }

        /// <summary>
        /// Converts an <see cref="ProductBrand"/> to <see cref="ProductBrandDTO"/>
        /// </summary>
        private ProductBrandDTO BrandToDTO(ProductBrand brand)
        {
            ProductBrandDTO brandDTO = new ProductBrandDTO()
            {
                brand_id = brand.BrandID,
                brand_name = brand.Name
            };

            return brandDTO;
        }
    }
}
