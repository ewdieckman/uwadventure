﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for orders
    /// </summary>
    public class Order
    {
        public Order(int orderNumber, Customer customer, Staff whoEnteredOrder, OrderStatus status, DateTime orderDate, Store store) {
            this.OrderNumber = orderNumber;
            this.Customer = customer;
            this.EnteredOrder = whoEnteredOrder;
            this.Status = status;
            this.OrderDate = orderDate;
            this.Store = store;
            this._items = new List<OrderItem>();
        }

        public int OrderNumber { get; private set; }
        public Customer Customer { get; private set; }
        public Staff EnteredOrder { get; private set; }
        public OrderStatus Status { get; private set; }
        public DateTime OrderDate { get; private set; }
        public DateTime? ShippedDate { get; private set; }
        public Store Store { get; private set; }
        public IEnumerable<OrderItem> Items { get { return _items; } }
        private IList<OrderItem> _items { get; set; }

        /// <summary>
        /// Adds an item to the order
        /// </summary>
        /// <remarks>Throws an exception if the product is already in the order.</remarks>
        public void AddItemToOrder(Product product, int quantity, decimal price)
        {

            //based on our database design (which should be a direct reflection of business rules), we can only have
            //1 item per product in the order.   Based on business rules, we could either throw an error if
            //someone is trying to add the same product twice OR we could simply increase the quantity of the product
            //already in the order.   Our business rule will be "throw an error".  We don't want to affect quantity
            //by the add item method

            //look for any products already in the order (we're going to use Linq)
            if (_items.Where(x => x.Product == product).Count() > 0)
            {
                throw new Exception(string.Format("Product {0} already in this order.  Increase or decrease the quantity.", product.Name));
            }

            OrderItem item = new OrderItem(this, product, quantity, price);

            _items.Add(item);
        }

        /// <summary>
        /// Removes a product from the order
        /// </summary>
        public void RemoveItemFromOrder(Product product)
        {

        }

        /// <summary>
        /// Increases the quantity of a product already in the order
        /// </summary>
        /// <param name="quantityToIncrease">Amount to increase the quantity of the product in the order</param>
        /// <remarks>Throws an exception if the product isn't in the order</remarks>
        public void IncreaseQuantity(Product product, int quantityToIncrease)
        {
            //looks to make sure the product is already in the order - throws an exception if it isn't
            OrderItem item = _items.Where(x => x.Product == product).First();
            if (item == null)
            {
                throw new Exception(string.Format("Product {0} is not included in this order.", product.Name));
            }

            item.IncreaseQuantity(quantityToIncrease);

        }

        /// <summary>
        /// Ships the order
        /// </summary>
        /// <remarks>Sets the shipping date and status to Completed</remarks>
        public void ShipOrder()
        {
            //you could make various checks before shipping, such as "is there an items in the order?", etc.
            ShippedDate = DateTime.Now;
            Status = OrderStatus.Completed;
        }

        /// <summary>
        /// Rejects an order
        /// </summary>
        public void RejectOrder()
        {
            Status = OrderStatus.Rejected;
        }
    }
}
