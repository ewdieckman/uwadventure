﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using UWAdventure.BLL;
using UWAdventure.Data;

namespace UWAdventure.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //setup configuration to mimic ASP.NET
            var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json");
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddScoped<IDbUWAdventure, DbUWAdventure>(
                    provider => new DbUWAdventure(
                        config
                    ))

                //register DAO objects
                .AddScoped<IOrderDAO, OrderDAO>()
                .AddScoped<IOrderItemDAO, OrderItemDAO>()
                .AddScoped<IStaffDAO, StaffDAO>()
                .AddScoped<IProductBrandDAO, ProductBrandDAO>()
                .AddScoped<IProductCategoryDAO, ProductCategoryDAO>()
                .AddScoped<IProductDAO, ProductDAO>()
                .AddScoped<IStoreDAO, StoreDAO>()
                .AddScoped<ICustomerDAO, CustomerDAO>()

                //register business services
                .AddScoped<ICreateOrderService, OrderService>()
                .AddScoped<IStaffService, StaffService>()
                .AddScoped<IStoreService, StoreService>()
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<IProductService, ProductService>()
                .AddScoped<IProductBrandService, ProductBrandService>()
                .AddScoped<IProductCategoryService, ProductCategoryService>()
                .AddScoped<IStoreService, StoreService>()



                .BuildServiceProvider();


            var dao = serviceProvider.GetService<IStaffDAO>();
            var obj = dao.GetByID(8);

            Console.WriteLine(obj.first_name);


            Console.ReadKey();

        }
    }
}
