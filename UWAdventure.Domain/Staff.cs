﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for staff
    /// </summary>
    public class Staff
    {
        public Staff(int staffID, string firstName, string lastName, string email, bool isActive, Store store, Staff manager)
        {
            StaffID = staffID;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            IsActive = IsActive;
            Store = store;
            Manager = manager;
        }

        public int StaffID { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; set; }
        public bool IsActive { get; private set; }
        public Store Store { get; private set; }
        public Staff Manager { get; private set; }
    }
}
