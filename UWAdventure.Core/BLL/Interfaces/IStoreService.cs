﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface IStoreService
    {
        Store GetByID(int store_id);
    }
}