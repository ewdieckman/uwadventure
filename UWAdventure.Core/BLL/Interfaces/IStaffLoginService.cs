﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.BLL
{
    public interface IStaffLoginService
    {
        bool AuthenticateStaff(int staff_id, short pin);
    }
}
