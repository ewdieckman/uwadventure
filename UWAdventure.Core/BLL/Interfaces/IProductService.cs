﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface IProductService
    {
        Product GetByID(int product_id);
    }
}