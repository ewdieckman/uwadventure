﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for order items
    /// </summary>
    public class OrderItemDAO : IOrderItemDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public OrderItemDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }

        public IEnumerable<OrderItemDTO> GetOrderItems(int order_number)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Persist order items in the persistence store
        /// </summary>
        public void Save(IList<OrderItemDTO> orderItems, int order_number)
        {
            //use MERGE command to deal with checking for existing items - takes care of INSERT, UPDATE, and DELETE in one command

            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                SqlCommand command = connection.CreateCommand();

                string commandText = String.Empty;
                if (orderItems.Count > 0)
                {

                    //set the order number from above using the first in the list (all should be the 

                    commandText = "SELECT * FROM (VALUES";
                    string strValues = "";

                    //construct a Table Value Constructor as the SOURCE using the input values
                    for (Int32 i = 0; i < orderItems.Count; i++)
                    {
                        if (strValues != "") { strValues += ","; };

                        strValues += String.Format("(@order_number, @product_id{0}, @quantity{0}, @price{0})", i);

                        //parameters for FROM values - be sure to include {0} and "i" in the parameter name
                        command.Parameters.Add(new SqlParameter(String.Format("@product_id{0}", i), SqlDbType.Int)).Value = orderItems[i].product_id;
                        command.Parameters.Add(new SqlParameter(String.Format("@quantity{0}", i), SqlDbType.Int)).Value = orderItems[i].quantity;
                        command.Parameters.Add(new SqlParameter(String.Format("@price{0}", i), SqlDbType.Decimal)).Value = orderItems[i].price;

                    }

                    strValues += ") AS supl_values(order_number, product_id, quantity, price)";
                    commandText += strValues;
                }
                else
                {
                    //no order items - still need a statement to return an empty result for merge to work properly
                    commandText = "SELECT order_number, order_number, product_id, quantity, price [sales].[order_items] WHERE 1=0";
                }

                //creates the order_number parameter to be used for all items.
                command.Parameters.Add(new SqlParameter("@order_number", SqlDbType.NVarChar, 80)).Value = order_number;

                commandText = @"MERGE [sales].[order_items] AS TARGET 
                USING (" + commandText + @") AS SOURCE
                ON TARGET.order_number = SOURCE.order_number AND TARGET.product_id = SOURCE.product_id
                WHEN MATCHED THEN
                UPDATE SET TARGET.quantity=SOURCE.quantity, TARGET.price=SOURCE.price
                WHEN NOT MATCHED BY TARGET THEN
                INSERT (order_number, product_id, quantity, price) VALUES (SOURCE.order_number, SOURCE.product_id, SOURCE.quantity, SOURCE.price)
                WHEN NOT MATCHED BY SOURCE AND TARGET.order_number=@order_number THEN
                DELETE;";

                command.CommandText = commandText;

                command.ExecuteNonQuery();
            }
                
            



        }
    }
}
