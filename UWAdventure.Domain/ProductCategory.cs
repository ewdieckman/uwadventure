﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for product categories
    /// </summary>
    public class ProductCategory
    {
        public ProductCategory(int categoryID, string name)
        {
            CategoryID = categoryID;
            Name = name;
        }

        public int CategoryID { get; private set; }
        public string Name { get; private set; }
    }
}
