﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerDAO _customerDAO;

        public CustomerService(ICustomerDAO customerDAO)
        {
            _customerDAO = customerDAO;
        }

        /// <summary>
        /// returns a <see cref="Customer"/> object for the specified customer ID
        /// </summary>
        public Customer GetByID(int store_id)
        {
            CustomerDTO storeDTO = _customerDAO.GetByID(store_id);
            return DTOtoStore(storeDTO);
        }

        /// <summary>
        /// Converts an <see cref="CustomerDTO"/> to <see cref="Customer"/>
        /// </summary>
        private Customer DTOtoStore(CustomerDTO customerDTO)
        {
            Customer customer = new Customer(customerDTO.customer_id, customerDTO.first_name, customerDTO.last_name, customerDTO.phone, 
                customerDTO.email, customerDTO.street, customerDTO.city, customerDTO.state, customerDTO.zip_code);

            return customer;
        }

        /// <summary>
        /// Converts an <see cref="Customer"/> to <see cref="CustomerDTO"/>
        /// </summary>
        private CustomerDTO StoreToDTO(Customer customer)
        {
            CustomerDTO customerDTO = new CustomerDTO()
            {
                customer_id = customer.CustomerID,
                first_name = customer.FirstName,
                last_name = customer.LastName,
                phone = customer.Phone,
                email = customer.Email,
                street = customer.Street,
                city = customer.City,
                state = customer.State,
                zip_code = customer.ZipCode,
            };

            return customerDTO;
        }
    }
}
