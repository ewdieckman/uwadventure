﻿using UWAdventure.Domain;

namespace UWAdventure.BLL
{
    public interface ICustomerService
    {
        Customer GetByID(int customer_id);
    }
}