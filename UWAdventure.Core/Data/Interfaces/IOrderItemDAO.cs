﻿using System.Collections.Generic;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IOrderItemDAO
    {
        IEnumerable<OrderItemDTO> GetOrderItems(int order_number);
        void Save(IList<OrderItemDTO> orderItems, int order_number);
    }
}