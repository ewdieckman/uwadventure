﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface IProductCategoryDAO
    {
        ProductCategoryDTO GetByID(int category_id);
    }
}