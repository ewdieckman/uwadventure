﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    public class StoreService : IStoreService
    {
        private readonly IStoreDAO _storeDAO;

        public StoreService(IStoreDAO storeDAO)
        {
            _storeDAO = storeDAO;
        }

        /// <summary>
        /// returns a <see cref="Store"/> object for the specified store ID
        /// </summary>
        public Store GetByID(int store_id)
        {
            StoreDTO storeDTO = _storeDAO.GetByID(store_id);
            return DTOtoStore(storeDTO);
        }

        /// <summary>
        /// Converts an <see cref="StoreDTO"/> to <see cref="Store"/>
        /// </summary>
        private Store DTOtoStore(StoreDTO storeDTO)
        {
            Store brand = new Store(storeDTO.store_id, storeDTO.store_name,storeDTO.phone, storeDTO.email,storeDTO.street, storeDTO.city, storeDTO.state, storeDTO.zip_code);

            return brand;
        }

        /// <summary>
        /// Converts an <see cref="Store"/> to <see cref="StoreDTO"/>
        /// </summary>
        private StoreDTO StoreToDTO(Store brand)
        {
            StoreDTO storeDTO = new StoreDTO()
            {
                store_id = brand.StoreID,
                store_name = brand.Name,
                phone = brand.Phone,
                email = brand.Email,
                street = brand.Street,
                city = brand.City,
                state = brand.State,
                zip_code = brand.ZipCode,
            };

            return storeDTO;
        }
    }
}
