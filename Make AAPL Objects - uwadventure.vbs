'##  Creates a DTO, Persister, and Mapper based on the SQL table it will be updating
'##
'##  Author: Eric Dieckman
'##  Date: 6/12/2016
'##
'#########################

'db = InputBox("Which database for the SQL parameters?", "Specify database")
db = "uwadventure"
tbl = InputBox("Which table to generate the AAPL classes from (include schema)?", "Specify table")
dtoName = InputBox("What will the DTO's class name be?", "Specify class name")
dtoSchema = InputBox("What is the base namespace for the DTO and all other AAPL objects?", "Specify namespace")
'tbl = "MusicTrip.musicals"
'dtoName = "MusicalDTO"
'dtoSchema = "RV.MusicTrip"

If db = "" OR tbl = "" OR dtoName = "" OR dtoSchema = "" Then Wscript.Quit

schema = Left(tbl, Instr(tbl, ".")-1)
tbl = Mid(tbl, Instr(tbl, ".")+1)

'Constants for recordsets
Public Const adUseClient = 3
Public Const adUseServer = 2
Public Const adForwardOnly = 0
Public Const adStateOpen = 1
Public Const adStateClosed = 0

'Constants for text files
Public Const adForReading = 1
Public Const adForWriting = 2
Public Const adForAppending = 8

'Constants for MsgBox

'Constants used for AD
Public Const ADS_SCOPE_SUBTREE = 2
Set objShell = WScript.CreateObject("WScript.Shell")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set chConn = CreateObject("ADODB.Connection")
chConn.Open "Provider=SQLNCLI11;Server=localhost;Database=" & db & ";Trusted_Connection=yes;"


Set rsCols = chConn.Execute("SELECT * FROM dbo.vwAllColumns WHERE TABLE_NAME='" & tbl & "' AND [SCHEMA_NAME]='" & schema & "' ORDER BY column_id;")

If rsCols.EOF Then
	Wscript.Echo "No table information found for " & schema & "." & tbl
	Wscript.Quit
End If

dto = ""
persist = ""
mapper = ""
service = ""
UNIQUEID = ""

'########################################################################
'###   DTO SECTION

'set the file name to DTO name (or table name, if not specified)
If dtoName = "" Then
	dtoFilename= tbl
	dtoName = tbl
Else
	dtoFilename =  dtoName
End If

'set filenames for the Create And Update DTOs
dtoCreateFilename = "Create" & dtoFileName
dtoUpdateFilename = "Update" & dtoFileName

dto = "using System;" & vbCrLf & "using System.Runtime.Serialization;" & vbCrLf & vbCrLf & "namespace " & dtoSchema
AppendLine dto, vbCrLf & "{" & vbCrLf & VSTab(1) & "[DataContract]" & vbCrLf & VSTab(1) & "public class " & dtoName & vbCrLf & VSTab(1) & "{", 1
AppendLine dto, "###DTO_PROPERTIES###", 0
AppendLine dto, VSTab(1) & "}" & vbCrLf & "}", 1

'########################################################################
'###   PERSISTER SECTION

'set the file name to DTO name (or table name, if not specified) with Persister in it
persistFilename = dtoName & "Persister"

persist = "using System;" & vbCrLf
AppendLine persist, "using System.Data;",1
AppendLine persist, "using AcidCreek.Core.Data;", 2
AppendLine persist, "namespace " & dtoSchema & ".Persistence" & vbCrLf & "{", 1
AppendLine persist, VSTab(1) & "internal class " & persistFilename, 1
AppendLine persist, VSTab(1) & "{", 1
AppendLine persist, VSTab(2) & "DBDao dao = new Data.DaoXXXX();", 2
AppendLine persist, VSTab(2) & "public Int32 Save(ref " & dtoCreateFilename & " entity)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, VSTab(3) & "Int32 uniqueID = Insert(entity);", 1
AppendLine persist, VSTab(3) & "return uniqueID;", 1
AppendLine persist, VSTab(2) & "}", 2
AppendLine persist, VSTab(2) & "public Int32 Save(ref " & dtoUpdateFilename & " entity)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, VSTab(3) & "Update(entity);", 1
AppendLine persist, VSTab(3) & "return entity.###UNIQUEID###;", 1
AppendLine persist, VSTab(2) & "}", 2
AppendLine persist, VSTab(2) & "private Int32 Insert(ref " & dtoCreateFilename & " entity)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, VSTab(3) & "string commandText = ""###INSERT_SQL###"";", 2
AppendLine persist, VSTab(3) & "commandText += ""SELECT SCOPE_IDENTITY();"";", 2
AppendLine persist, VSTab(3) & "IDbCommand command = dao.GetDbCommand(commandText);", 1
AppendLine persist, VSTab(3) & "SetCommonProperties(command, entity);", 1
'AppendLine persist, "###INSERT_PARAMETERS###", 1
AppendLine persist, VSTab(3) & "return dao.ExecuteScalar(command);", 1
AppendLine persist, VSTab(2) & "}", 2
AppendLine persist, VSTab(2) & "private void Update(ref " & dtoUpdateFilename & " entity)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, VSTab(3) & "string commandText = ""###UPDATE_SQL###"";", 2
AppendLine persist, VSTab(3) & "IDbCommand command = dao.GetDbCommand(commandText);", 1
AppendLine persist, VSTab(3) & "SetCommonProperties(command, entity);", 1
'AppendLine persist, "###UPDATE_PARAMETERS###", 1
AppendLine persist, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@uniqueID"", entity.###UNIQUEID###));", 1
AppendLine persist, VSTab(3) & "dao.ExecuteNonQuery(command);", 1
AppendLine persist, VSTab(2) & "}", 2
AppendLine persist, VSTab(2) & "private void SetCommonProperties(ref IDbCommand command, ref " & dtoUpdateFilename & " entity)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, "###UPDATE_PARAMETERS###", 1
AppendLine persist, VSTab(2) & "}", 2
AppendLine persist, VSTab(2) & "public void Delete(Int32 uniqueID)", 1
AppendLine persist, VSTab(2) & "{", 1
AppendLine persist, VSTab(3) & "string commandText = ""DELETE FROM [" & schema & "].[" & tbl & "] WHERE ###UNIQUEID###=@uniqueID;"";", 2
AppendLine persist, VSTab(3) & "IDbCommand command = dao.GetDbCommand(commandText);", 1
AppendLine persist, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@uniqueID"", uniqueID));", 1
AppendLine persist, VSTab(3) & "dao.ExecuteNonQuery(command);", 1
AppendLine persist, VSTab(2) & "}", 2

'########################################################################
'###   mAPPER SECTION

'set the file name to DTO name (or table name, if not specified) with Mapper in it
mapperFilename = dtoName & "Mapper"
mapper = "using System;" & vbCrLf
AppendLine mapper, "using AcidCreek.Core.Data.Mappers;", 2
AppendLine mapper, "namespace " & dtoSchema & ".DataMappers", 1
AppendLine mapper, "{", 1
AppendLine mapper, VSTab(1) & "internal class " & mapperFilename & " : IDataMapper", 1
AppendLine mapper, VSTab(1) & "{", 2
AppendLine mapper, VSTab(2) & "private bool m_isInitialized = false;", 1
AppendLine mapper, "###PRIVATE_ORDINALS###", 2
AppendLine mapper, VSTab(2) & "private void InitializeMapper(System.Data.IDataReader reader)", 1
AppendLine mapper, VSTab(2) & "{", 1
AppendLine mapper, VSTab(3) & "PopulateOrdinals(reader);", 1
AppendLine mapper, VSTab(3) & "m_isInitialized = true;", 1
AppendLine mapper, VSTab(2) & "}", 2
AppendLine mapper, VSTab(2) & "public void PopulateOrdinals(System.Data.IDataReader reader)", 1
AppendLine mapper, VSTab(2) & "{", 1
AppendLine mapper, "###POPULATE_ORDINALS###", 1
AppendLine mapper, VSTab(2) & "}", 2
AppendLine mapper, VSTab(2) & "Object IDataMapper.Map(System.Data.IDataReader reader) ", 1
AppendLine mapper, VSTab(2) & "{ ", 1
AppendLine mapper, VSTab(3) & "if (!m_isInitialized) { InitializeMapper(reader); }", 1
AppendLine mapper, VSTab(3) & dtoFilename & " entity = new " & dtoFilename & "();", 2
AppendLine mapper, "###MAP_PROPERTIES###", 1
AppendLine mapper, VSTab(3) & "return entity;", 1
AppendLine mapper, VSTab(2) & "}", 2

'########################################################################
'###   SERVICE SECTION

'set the file name to DTO name (or table name, if not specified) with Service in it
serviceFilename = Replace(dtoName, "DTO", "") & "Service"

service = "using System;" & vbCrLf
AppendLine service, "using System.Data;", 1
AppendLine service, "using AcidCreek.Core.Data;", 1
AppendLine service, "using AcidCreek.Core.Data.Mappers;", 1
AppendLine service, "using " & dtoSchema & ".DataMappers;", 1
AppendLine service, "using " & dtoSchema & ".Persistence;", 2
AppendLine service, "namespace " & dtoSchema & ".Services", 1
AppendLine service, "{", 1
AppendLine service, VSTab(1) & "public class " & serviceFilename, 1
AppendLine service, VSTab(1) & "{", 1
AppendLine service, VSTab(2) & "private readonly " & persistFilename & " Persister = null;", 1
AppendLine service, VSTab(2) & "private readonly IDataMapperFactory MapperFactory = null;", 1
AppendLine service, VSTab(2) & "DBDao dao = new Data.DaoXXXX();", 2
AppendLine service, VSTab(2) & "/// <summary>", 1
AppendLine service, VSTab(2) & "/// constructor", 1
AppendLine service, VSTab(2) & "/// </summary>", 1
AppendLine service, VSTab(2) & "public " & serviceFilename & "()", 1
AppendLine service, VSTab(2) & "{", 1
AppendLine service, VSTab(3) & "Persister = new " & persistFilename & "();", 1
AppendLine service, VSTab(3) & "MapperFactory = new XXXXMapperFactory();", 1
AppendLine service, VSTab(2) & "}", 2
AppendLine service, VSTab(2) & "/// <summary>", 1
AppendLine service, VSTab(2) & "/// persist the entity", 1
AppendLine service, VSTab(2) & "/// </summary>", 1
AppendLine service, VSTab(2) & "public void Save(ref " & dtoCreateFilename & " entity)", 1
AppendLine service, VSTab(2) & "{", 1
AppendLine service, VSTab(3) & "Persister.Save(entity);", 1
AppendLine service, VSTab(2) & "}", 2
AppendLine service, VSTab(2) & "/// <summary>", 1
AppendLine service, VSTab(2) & "/// persist the entity", 1
AppendLine service, VSTab(2) & "/// </summary>", 1
AppendLine service, VSTab(2) & "public void Save(ref " & dtoUpdateFilename & " entity)", 1
AppendLine service, VSTab(2) & "{", 1
AppendLine service, VSTab(3) & "Persister.Save(entity);", 1
AppendLine service, VSTab(2) & "}", 2
AppendLine service, VSTab(2) & "/// <summary>", 1
AppendLine service, VSTab(2) & "/// Returns a <see cref=""" & dtoFilename & """ /> with the specified UniqueID", 1
AppendLine service, VSTab(2) & "/// </summary>", 1
AppendLine service, VSTab(2) & "public " & dtoFilename & " GetDTO(Int32 uniqueID)", 1
AppendLine service, VSTab(2) & "{", 1
AppendLine service, VSTab(3) & "IDbCommand command = dao.GetDbCommand(""SELECT * FROM [" & schema & "].[" & tbl & "] WHERE ###UNIQUEID###=@uniqueID;"");", 1
AppendLine service, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@uniqueID"", uniqueID));", 1
AppendLine service, VSTab(3) & "return dao.GetSingle<" & dtoFilename & ">(command, MapperFactory);", 1
AppendLine service, VSTab(2) & "}", 2

While Not rsCols.EOF

	'dont do it for tmAdd, tmUpdate, which is what is typically used for persistence properties
	columnName = rsCols.Fields("COLUMN_NAME")
	dataType = rsCols.Fields("DATA_TYPE")
	maxLength = rsCols.Fields("CHARACTER_MAXIMUM_LENGTH")
	isIdentity = rsCols.Fields("is_identity")
	isComputed = rsCols.Fields("is_computed")
	isNullable = rsCols.Fields("is_nullable")
	collation_name = rsCols.Fields("collation_name")
	isUnique = rsCols.Fields("is_unique")

	m_oridinal = "m_ordinal_" & columnName

	'reset daoParam - used once for each parameter, then reset.  daoCreateParam and daoUpdateParam hold running values
	daoParam = ""
	dtoPropsTemp = ""
	If LCase(columnName) <> "tmadd" And LCase(columnName) <> "tmupdate" AND LCase(columnName) <> "tm_add" And LCase(columnName) <> "tm_update"  Then

		'data contract
		AppendLine dtoPropsTemp, VSTab(2) & "[DataMember]", 1
		AppendLine dtoPropsTemp, VSTab(2) & "public " & SQL_to_CS_type(dataType) & " " & columnName & " { get; set; } = " & DefaultValue(dataType) & ";", 2

		If Not isIdentity Then


			If updateSQL <> "" Then AppendLine updateSQL, ", ", 0
			AppendLine updateSQL, columnName & "=@" & columnName, 0

			If insertParm <> "" Then AppendLine insertParm, ", ", 0
			AppendLine insertParm, "@" & columnName, 0


			AppendLine daoParam, VSTab(3) & "command.Parameters.Add(dao.CreateParameter(""@" & columnName & """, entity." & columnName, 0
			If Not IsNull(maxLength) AND dataType <> "text" AND Not ISNull(collation_name) Then
				AppendLine daoParam, ", " & maxLength, 0
			End If

			If  Instr(LCase(columnName), "date") <> 0 Then
				AppendLine daoParam, ", DbType." & dataType, 0
			End If


			AppendLine daoParam, "));", 1
		
			daoCreateParam = daoCreateParam & daoParam
			daoUpdateParam = daoUpdateParam & daoParam
			dtoCreateProps = dtoCreateProps & dtoPropsTemp
			dtoUpdateProps = dtoUpdateProps & dtoPropsTemp
			
		End If
		
		dtoProps = dtoProps & dtoPropsTemp
	Else
		'special cases for tmAdd and tmUpdate
		If LCase(columnName) <> "tmadd" Then
			If updateSQL <> "" Then AppendLine updateSQL, ", ", 0
			AppendLine updateSQL, columnName & "=SYSUTCDATETIME()", 0
		End If

	End If

	If Not isIdentity Then
		If insertCol <> "" Then AppendLine insertCol, ", ", 0
		AppendLine insertCol, columnName, 0
	End If

	AppendLine privateOrdinal,  VSTab(2) & "private Int32 " & m_oridinal & ";", 1

	AppendLine populateOrdinal, VSTab(3) & m_oridinal & " = reader.GetOrdinal(""" & columnName & """);", 1

	AppendLine mapFields, VSTab(3) & "if (!reader.IsDBNull(" & m_oridinal & ")) { entity." & columnName & " = reader.Get" & SQL_to_CS_type(dataType) & "(" & m_oridinal & "); }", 1


	'replace uniqueID references in the files
	If isUnique Then
		UNIQUEID = columnName
	End If

	rsCols.MoveNext
Wend

updateSQL = "UPDATE [" & schema & "].[" & tbl & "] SET " & updateSQL & " WHERE ###UNIQUEID###=@uniqueID;"
insertSQL = "INSERT INTO [" & schema & "].[" & tbl & "] (" & insertCol & ") VALUES (" & insertParm & ");"

mapper = Replace(mapper, "###PRIVATE_ORDINALS###", privateOrdinal)
mapper = Replace(mapper, "###POPULATE_ORDINALS###", populateOrdinal)
mapper = Replace(mapper, "###MAP_PROPERTIES###", mapFields)
persist = Replace(persist, "###INSERT_SQL###", insertSQL)
persist = Replace(persist, "###UPDATE_SQL###", updateSQL)
persist = Replace(persist, "###UPDATE_PARAMETERS###", daoCreateParam)
'persist = Replace(persist, "###INSERT_PARAMETERS###", daoCreateParam)
'persist = Replace(persist, "###UPDATE_PARAMETERS###", daoUpdateParam)
dtoCreate = Replace(dto, "###DTO_PROPERTIES###", dtoCreateProps)
dtoCreate = Replace(dtoCreate, dtoName, dtoCreateFilename)
dtoUpdate = Replace(dto, "###DTO_PROPERTIES###", dtoUpdateProps)
dtoUpdate = Replace(dtoUpdate, dtoName, dtoUpdateFilename)
dto = Replace(dto, "###DTO_PROPERTIES###", dtoProps)

AppendLine persist, VSTab(1) & "}" & vbCrLf & "}", 0	'End Class and End Namespace
AppendLine mapper, VSTab(1) & "}" & vbCrLf & "}", 0		'End Class and End Namespace
AppendLine service, VSTab(1) & "}" & vbCrLf & "}", 0	'End Class and End Namespace

If UNIQUEID <> "" Then
	service = Replace(service, "###UNIQUEID###", UNIQUEID)
	persist = Replace(persist, "###UNIQUEID###", UNIQUEID)
	dto = Replace(dto, "###UNIQUEID###", UNIQUEID)
End If



'########################################################################
'###   FINAL FILE WRITING
'###

strDesktop = objShell.SpecialFolders("Desktop")
objFSO.CreateFolder(strDesktop & "\Models")
objFSO.CreateFolder(strDesktop & "\Models\View")
objFSO.CreateFolder(strDesktop & "\Models\Input")
Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\View\" & dtoFilename & ".cs", True)
objFLwrite.WriteLine dto
objFLwrite.Close

Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\Input\" & dtoCreateFilename & ".cs", True)
objFLwrite.WriteLine dtoCreate
objFLwrite.Close

Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Models\Input\" & dtoUpdateFilename & ".cs", True)
objFLwrite.WriteLine dtoUpdate
objFLwrite.Close

objFSO.CreateFolder(strDesktop & "\Persistence")
Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Persistence\" & persistFilename & ".cs", True)
objFLwrite.WriteLine persist
objFLwrite.Close

objFSO.CreateFolder(strDesktop & "\DataMappers")
Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\DataMappers\" & mapperFilename & ".cs", True)
objFLwrite.WriteLine mapper
objFLwrite.Close

objFSO.CreateFolder(strDesktop & "\Services")
Set objFLwrite = objFSO.CreateTextFile(strDesktop & "\Services\" & serviceFilename & ".cs", True)
objFLwrite.WriteLine service
objFLwrite.Close

'clean up'
chConn.Close

MsgBox "Done.  Class files can be found here: " & strDesktop

'########################################################################
'###   FUNCTIONS
'###
Function SQL_to_CS_type(dataType)

	Select Case dataType
		Case "int"
			SQL_to_CS_type = "Int32"
		Case "smallint"
			SQL_to_CS_type = "Int16"
		Case "bigint"
			SQL_to_CS_type = "Int64"
		Case "bit"
			SQL_to_CS_type = "bool"
		Case "decimal"
			SQL_to_CS_type = "Decimal"
		Case "date", "datetime", "datetime2"
			SQL_to_CS_type = "DateTime"
		Case "varchar", "text", "nvarchar", "char"
			SQL_to_CS_type = "String"
		Case "float"
			SQL_to_CS_type = "Double"
		Case "uniqueidentifier"
			SQL_to_CS_type = "Guid"


	End Select

End Function

Function DefaultValue(dataType)

	Select Case dataType
		Case "int", "smallint", "bigint", "decimal", "float"
			DefaultValue = "-1"
		Case "bit"
			DefaultValue = "true"
		Case "date", "datetime", "datetime2", "uniqueidentifier"
			DefaultValue = "null"
		Case "varchar", "text", "nvarchar", "char"
			DefaultValue = """"""
	End Select

End Function

'returns number of spaces based on the number of tabs (to mimic Visual Studio)
Function VSTab(num)
	str = ""
	For i = 1 To num
		str = str & "    "
	Next

	VSTab = str
End Function

'appends text to a string variable'
Sub AppendLine(inStr, strToAdd, num_carriage_return)
	If IsEmpty(num_carriage_return) Then num_carriage_return = 0

	inStr = inStr & strToAdd
	For i = 1 To num_carriage_return
		inStr = inStr & vbCrLf
	Next
End Sub
