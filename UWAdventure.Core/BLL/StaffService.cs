﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    /// <summary>
    /// Business logic for dealing with staff
    /// </summary>
    public class StaffService : IStaffService, IStaffLoginService
    {

        private readonly IStaffDAO _staffDAO;
        private readonly IStoreService _storeService;

        public StaffService(IStaffDAO staffDAO, IStoreService storeService)
        {
            _staffDAO = staffDAO;
            _storeService = storeService;
        }

        /// <summary>
        /// returns a <see cref="Staff"/> object for the specified staff ID
        /// </summary>
        public Staff GetByID(int staff_id)
        {
            StaffDTO staffDTO = _staffDAO.GetByID(staff_id);
            return DTOtoStaff(staffDTO);
        }

        /// <summary>
        /// Returns whether the supplied PIN matches the stored pin for the specified user
        /// </summary>
        /// <param name="staff_id"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        public bool AuthenticateStaff(int staff_id, short pin)
        {
            short stored_pin = _staffDAO.GetPINforStaff(staff_id);
            return stored_pin == pin;
        }

        /// <summary>
        /// Converts an <see cref="StaffDTO"/> to <see cref="Staff"/>
        /// </summary>
        private Staff DTOtoStaff(StaffDTO staffDTO)
        {

            Store store = _storeService.GetByID(staffDTO.store_id);
            Staff manager = null;
            if (staffDTO.manager_id.HasValue)
            {
                manager = GetByID(staffDTO.manager_id.Value);
            }

            Staff staff = new Staff(staffDTO.staff_id, staffDTO.first_name, staffDTO.last_name, staffDTO.email, staffDTO.active, store, manager);
            staff.Phone = staffDTO.phone;

            return staff;
        }

        /// <summary>
        /// Converts an <see cref="Staff"/> to <see cref="StaffDTO"/>
        /// </summary>
        private StaffDTO StaffToDTO(Staff staff)
        {
            StaffDTO staffDTO = new StaffDTO()
            {
                store_id = staff.Store.StoreID,
                staff_id = staff.StaffID,
                first_name = staff.FirstName,
                last_name = staff.LastName,
                active = staff.IsActive,
                email = staff.Email,
                manager_id = staff.Manager.StaffID,
                phone  = staff.Phone
            };

            return staffDTO;
        }

    }
}
