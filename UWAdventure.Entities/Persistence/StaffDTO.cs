using System;
using System.Runtime.Serialization;

namespace UWAdventure.Entities.Persistence
{
    /// <summary>
    /// DTO used for staff being persisted
    /// </summary>
    public class StaffDTO
    {

        public static StaffDTO NOTFOUND = new StaffDTO
        {
            last_name = "NOT FOUND",
            first_name = "NOT FOUND"

        };

        public int staff_id { get; set; } = -1;
        public string first_name { get; set; } = "";
        public string last_name { get; set; } = "";
        public string email { get; set; } = "";
        public string phone { get; set; } = "";
        public bool active { get; set; } = false;
        public int store_id { get; set; } = -1;
        public int? manager_id { get; set; } = -1;

    }
}

