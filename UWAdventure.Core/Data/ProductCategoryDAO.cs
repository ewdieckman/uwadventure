﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using UWAdventure.Entities.Persistence;
using Dapper;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for product category
    /// </summary>
    public class ProductCategoryDAO : IProductCategoryDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public ProductCategoryDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }

        public ProductCategoryDTO GetByID(int category_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [production].[categories] WHERE category_id=@category_id;";
                ProductCategoryDTO category = connection.QuerySingleOrDefault<ProductCategoryDTO>(sql, new { category_id });
                if (category == null)
                {
                    //returning the static NOT FOUND object for ProductCategoryDTO.  Easy to compare, plus more obvious what is means:
                    // if (mycategory = ProductCategoryDTO.NOTFOUND)  compared to  if (mycategory == null) - the former is clearer as to what is happening
                    return ProductCategoryDTO.NOTFOUND;
                }
                return category;
            }
        }
    }
}
