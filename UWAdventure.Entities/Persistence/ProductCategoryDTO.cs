﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Entities.Persistence
{
    public class ProductCategoryDTO
    {

        public static ProductCategoryDTO NOTFOUND = new ProductCategoryDTO
        {
            category_name = "NOT FOUND"
        };


        public int category_id { get; set; } = -1;
        public string category_name { get; set; } = "";
    }
}
