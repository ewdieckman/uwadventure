﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for stores
    /// </summary>
    public class Store
    {
        public Store(int storeID, string name, string phone, string email, string street, string city, string state, string zip)
        {
            StoreID = storeID;
            Name = name;
            Phone = phone;
            Email = email;
            Street = street;
            City = city;
            State = state;
            ZipCode = zip;
        }

        public int StoreID { get; private set; }
        public string Name { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public string Street { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string ZipCode { get; private set; }
    }
}
