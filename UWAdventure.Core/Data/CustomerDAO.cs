﻿using System.Data.SqlClient;
using UWAdventure.Entities.Persistence;
using Dapper;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for customers
    /// </summary>
    public class CustomerDAO : ICustomerDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public CustomerDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }

        public CustomerDTO GetByID(int customer_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [sales].[staffs] WHERE store_id=@store_id;";
                CustomerDTO customer = connection.QuerySingleOrDefault<CustomerDTO>(sql, new { customer_id });
                if (customer == null)
                {
                    //returning the static NOT FOUND object for CustomerDTO.  Easy to compare, plus more obvious what is means:
                    // if (mycustomer = CustomerDTO.NOTFOUND)  compared to  if (mycustomer == null) - the former is clearer as to what is happening
                    return CustomerDTO.NOTFOUND;
                }
                return customer;
            }
        }
    }
}
