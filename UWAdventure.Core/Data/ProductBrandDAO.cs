﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using UWAdventure.Entities.Persistence;
using Dapper;

namespace UWAdventure.Data
{
    /// <summary>
    /// Data access object for product brand
    /// </summary>
    public class ProductBrandDAO : IProductBrandDAO
    {
        private readonly IDbUWAdventure _dbObj;

        public ProductBrandDAO(IDbUWAdventure dbObj)
        {
            _dbObj = dbObj;
        }

        public ProductBrandDTO GetByID(int brand_id)
        {
            using (var connection = new SqlConnection(_dbObj.GetConnectionString("uwadventure")))
            {
                connection.Open();
                string sql = @"SELECT * FROM [production].[categories] WHERE brand_id=@brand_id;";
                ProductBrandDTO Brand = connection.QuerySingleOrDefault<ProductBrandDTO>(sql, new { brand_id });
                if (Brand == null)
                {
                    //returning the static NOT FOUND object for ProductBrandDTO.  Easy to compare, plus more obvious what is means:
                    // if (myBrand = ProductBrandDTO.NOTFOUND)  compared to  if (myBrand == null) - the former is clearer as to what is happening
                    return ProductBrandDTO.NOTFOUND;
                }
                return Brand;
            }
        }
    }
}
