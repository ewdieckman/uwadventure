﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Entities.Persistence
{
    /// <summary>
    /// DTO used for stores being persisted
    /// </summary>
    public class StoreDTO
    {
        public static StoreDTO NOTFOUND = new StoreDTO
        {
            store_name = "NOT FOUND"
        };

        public int store_id { get; set; } = -1;
        public string store_name { get; set; } = "";
        public string phone { get; set; } = "";
        public string email { get; set; } = "";
        public string street { get; set; } = "";
        public string city { get; set; } = "";
        public string state { get; set; } = "";
        public string zip_code { get; set; } = "";
    }
}
