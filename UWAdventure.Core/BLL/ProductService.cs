﻿using System;
using System.Collections.Generic;
using System.Text;
using UWAdventure.Data;
using UWAdventure.Domain;
using UWAdventure.Entities.Persistence;

namespace UWAdventure.BLL
{
    /// <summary>
    /// Business logic for dealing with product
    /// </summary>
    public class ProductService : IProductService
    {
        private readonly IProductDAO _productDAO;
        private readonly IProductBrandService _brandService;
        private readonly IProductCategoryService _categoryService;

        public ProductService(IProductDAO productDAO, IProductBrandService brandService, IProductCategoryService categoryService)
        {
            _productDAO = productDAO;
            _categoryService = categoryService;
            _brandService = brandService;
        }
        public Product GetByID(int product_id)
        {
            return DTOtoProduct(_productDAO.GetByID(product_id));
        }

        /// <summary>
        /// Converts an <see cref="ProductDTO"/> to <see cref="Product"/>
        /// </summary>
        private Product DTOtoProduct(ProductDTO productDTO)
        {
            ProductBrand brand = _brandService.GetByID(productDTO.brand_id);
            ProductCategory category = _categoryService.GetByID(productDTO.category_id);

            Product product = new Product(productDTO.product_id, productDTO.product_name, productDTO.list_price, productDTO.model_year, category, brand);

            return product;
        }

        /// <summary>
        /// Converts an <see cref="Product"/> to <see cref="ProductDTO"/>
        /// </summary>
        private ProductDTO ProductToDTO(Product product)
        {
            ProductDTO productDTO = new ProductDTO()
            {
                brand_id = product.Brand.BrandID,
                category_id = product.Category.CategoryID,
                product_id = product.ProductID,
                product_name = product.Name,
                list_price = product.ListPrice,
                model_year = product.ModelYear
            };

            return productDTO;
        }

    }
}
