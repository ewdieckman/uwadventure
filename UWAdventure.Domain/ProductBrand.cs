﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Domain
{
    /// <summary>
    /// Rich domain model for product brands
    /// </summary>
    public class ProductBrand
    {
        public ProductBrand(int brandID, string name)
        {
            BrandID = brandID;
            Name = name;
        }

        public int BrandID { get; private set; }
        public string Name { get; private set; }
    }
}
