﻿using UWAdventure.Entities.Persistence;

namespace UWAdventure.Data
{
    public interface ICustomerDAO
    {
        CustomerDTO GetByID(int customer_id);
    }
}