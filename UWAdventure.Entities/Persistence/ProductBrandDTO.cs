﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UWAdventure.Entities.Persistence
{
    /// <summary>
    /// DTO used for brand being persisted
    /// </summary>
    public class ProductBrandDTO
    {

        public static ProductBrandDTO NOTFOUND = new ProductBrandDTO
        {
            brand_name = "NOT FOUND"
        };

        public int brand_id { get; set; } = -1;
        public string brand_name { get; set; } = "";

    }
}
